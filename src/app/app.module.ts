import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SbextHeaderComponent } from './sbext-header/sbext-header.component';
import { SbextDashboardComponent } from './sbext-dashboard/sbext-dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    SbextHeaderComponent,
    SbextDashboardComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
