import { Component } from '@angular/core';
import { ManualService } from '../services/manual/manual.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'sbext-dashboard',
  templateUrl: './sbext-dashboard.component.html'
})
export class SbextDashboardComponent {
  searchText:any;
  startSearch:any;
  searching:boolean = false;
  AllK: any[] = [];
  constructor(private _manualService: ManualService) {}

	searchKnowledgeCard(event: any) {
		if (this.startSearch) clearTimeout(this.startSearch);
		this.startSearch = setTimeout(() => {
			this.searching = true;
			this._manualService
				.searchCardFilter({
					"library_id": 4,
					"manual_id": 51,
					"search": event,
					"page_limit": 10,
					"currentPage": 1,
					"tag_ids": [],
					"cat_ids": []
				})
				.pipe(first())
				.subscribe(
					data => {
						if (data.success) {
							this.AllK = data.success.data.rows;
						} else if (data.error) {
						
						}
						this.searching = false;
					},
					error => {
						this.searching = false;
					}
				);
		}, 1500);
	}

}
