// Listen for the extension's click
chrome.browserAction.onClicked.addListener(startExt);
function startExt(tab) {
    var tabId = tab ? tab.id : null; // Defaults to the current tab
    chrome.tabs.executeScript(tabId, {
        file: "content_script.js", // Script to inject into page and run in sandbox
        allFrames: false // This injects script into iframes in the page and doesn't work before 4.0.266.0.
    });

    chrome.tabs.sendMessage( tabId, {type:'start'}, (res) => {
        if (chrome.runtime.lastError) {
            //console.log(chrome.runtime.lastError);
        } else {
            //console.log(res);
        }
    })
}