// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { src, dest, watch, series, parallel } = require('gulp');
const concat = require('gulp-concat');

// File paths
const files = [
  'dist/upwork-chrome-extension/runtime.js',
  'dist/upwork-chrome-extension/es2015-polyfills.js',
  'dist/upwork-chrome-extension/polyfills.js',
  'dist/upwork-chrome-extension/main.js'
];

// JS task: concatenates and uglifies JS files to script.js
function jsTask(){
    return src(files)
        .pipe(concat('combined.js'))
        .pipe(dest('dist')
    );
}

// Watch task: watch JS files for changes
function watchTask(cb){
    watch(files, parallel(jsTask));
    cb();
}

// Export the default Gulp task so it can be run
// then runs cacheBust, then watch task
exports.default = series(
    parallel(jsTask), 
    watchTask
);