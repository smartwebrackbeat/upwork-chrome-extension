# Upwork Knowledg Base
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.6.

# Pre Requistics
Angular CLI, NodeJs.

# Preocess
Build angular build `ng build` which build project in `/dist`, copy `manifest.json`,  `content_script.js` and icons folder into `/dist` and upload this folder to chrome extension in developer mode.

# Basic scope of the project
The chrome extensio is uses pure javascript and html, css.
So, we can develope our project in angular and create a build from angular cli which convert all angular, typescript code into javascript, and then we can use this javascript code into our extension.
