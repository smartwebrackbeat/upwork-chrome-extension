chrome.runtime.onMessage.addListener( (request, sender, respond) => {
    var handler = new Promise( (resolve, reject) => {
        //console.log(sender.tab ? "from a content script:" + sender.tab.url : "from the extension");
        if(request.type == 'start') {
            var content = start();
            resolve(content);
        } else if (request.type == 'getanswer') {
            if (request.url == "upwork.expertrating.com") {
                var content = getanswerExpertrating();
            } else if (request.url == "www.upwork.com") {
                var content = getanswerUpwork();
            }            
            resolve(content);
        }  else {
            reject('request is empty.');
        }
    });
    handler.then(function (message) { return respond(message); }).catch( (error) => { return respond(error); });
    return true;
})

function getanswerExpertrating() {
    //console.log("collecting questiona and answers from Expertrating");
    if(document.getElementById("label_question") != null) {        
        var contentToSend = {};
        contentToSend.question = trimText(document.getElementById("label_question").textContent);
        var table = document.querySelectorAll('#divopt tr');
        var answersArr = [];
        for (var i = 0; i < table.length; i++) { 
            var c = (table[i]).cells;
            answersArr.push(trimText((c[c.length-1]).textContent));
        }
        contentToSend.answers = answersArr;
        return contentToSend;
    } else {
        return "Wrong destination";
    }	
}

function getanswerUpwork(){
    //console.log("collecting questiona and answers from Upwork");
    if(document.getElementById("questionForm") != null) {        
        var contentToSend = {};
        contentToSend.question = trimText(document.querySelector('#questionForm pre').textContent);
        var table = document.querySelectorAll('#answerOptions .oOptBox');
        var answersArr = [];
        for (var i = 0; i < table.length; i++) {
            answersArr.push(trimText(table[i].textContent));
        }
        contentToSend.answers = answersArr;
        return contentToSend;
    } else {
        return "Wrong destination";
    }   
}

function start() {
    //console.log("starting");
    if(document.getElementById("randomid") == null) {
        var content = chrome.runtime.getURL("index.html");
        var iframe = document.createElement('iframe');
        iframe.id="randomid";
        iframe.style = "width:400px;height:500px;position:fixed;top:10px;right:10px;z-index:99999;border:1px solid #f3f3f3;box-shadow:0 0 10px 0 rgba(112,112,112,.41)";
        iframe.src = content;
        document.body.appendChild(iframe);
    } else {
        document.body.removeChild(document.getElementById("randomid"));
    }
}

function trimText(text) {
    var t = text;
    t = t.replace(/(\r\n|\n|\r)/gm, "");
    t = t.trim();
    return t;
}
